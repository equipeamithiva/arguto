<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'arguto' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' ); 

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

define('WP_HOME','http://localhost/arguto');

define('WP_SITEURL','http://localhost/arguto');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */

define( 'AUTH_KEY',         '](XF}hr*UfeG>LYJ%v:KdM4WLqJX^G-u#F70Cg@zA2vhCdL{Hd<nLfgcN5_UA`{!' );
define( 'SECURE_AUTH_KEY',  'kIHW%Vpb&Z(@+~g0h3Y:fbT!eMhyp)N:1)2f5on5w:mV;nX>,/_Jt,obx|m:L[ls' );
define( 'LOGGED_IN_KEY',    'p76id:OqQvT9ogM?i<ij i,unD1;`WNjQy7F2)u=VbG.~k&FFyAh`~9h5nO=l>rn' );
define( 'NONCE_KEY',        'yyq|JS>_YcO;8Q7C(06j;giBTi#K~q&Jbafb~Ze}DA>t7 .xZ<E%EBeJc}a1V(GV' );
define( 'AUTH_SALT',        'P-EX|^ dO pC`ivf{^|Z<*7,OLUvgk0%1sJq,(5j n7Uyhhb,DICTxh,nR];AKI7' );
define( 'SECURE_AUTH_SALT', '=v(vqJ;oVL~CTP($Q8zuktUpyMB%v%~{!$!bI+2+59e.t=9Q6dyuCh/d<TsRxFSe' );
define( 'LOGGED_IN_SALT',   '_xe?JLTDg?`?h%/4a1/aspJX@A+L,u#k^jSoiPo{S&r}oqmosK>Jrx2<+p!^{66`' );
define( 'NONCE_SALT',       'S<H4`tEld>dzlvq1b;7G;Z&?`vWLAuL2<ve;Q3(gXeON0x[or~boZ~Wj$>_l.LY>' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');


