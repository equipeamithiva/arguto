
(function($){
  
  function goTo(element , speed){
    var href = element.attr("href");
    var top = $(href).offset().top;
    $("html,body").animate({ scrollTop : top}, speed);
  }

  $(function(){
    $("#linkagem a").click( function(e){
      e.preventDefault();
      goTo($(this), 500);
    })
  })


  $(".acordion_saiba").click(function(){
  elemento =  $(this);
    if(elemento.hasClass("acordion_open")){
        elemento.removeClass("acordion_open");
    }else{
        if(elemento.parent().find(".acordion_open")){
            elemento.parent().find(".acordion_open").removeClass("acordion_open");
            elemento.addClass("acordion_open");
        }        
    }
  })


    //EFEITO MENU
    var _rys = jQuery.noConflict();
    _rys("document").ready(function(){
      _rys(window).scroll(function () { 
        if (_rys(this).scrollTop() > 150) {
          _rys('.settings .header').addClass("mostrar");
        } else {
          _rys('.settings .header').removeClass("mostrar");
        }
      });
    });

    $(".menu-toggle").on('click', function() {
      $(this).toggleClass("on");
      $('.menu-section').toggleClass("on");
      $("nav").toggleClass('hidden');
      if (window.matchMedia('@media(min-width: map-get($grid-breakpoints,lg))').matches){ 
       $('nav ul').css('transform','translateY(-10%)');
     }
     $('nav ul').addClass('fadeIn');
   });

    //SCROLL TO TOP
    if ($('#back-to-top').length) {
      var scrollTrigger = 100, // px
      backToTop = function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > scrollTrigger) {
          $('#back-to-top').addClass('show');
        } else {
          $('#back-to-top').removeClass('show');
        }
      };
      backToTop();
      $(window).on('scroll', function () {
        backToTop();
      });
      $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
          scrollTop: 0
        }, 700);
      });
    }
    //DEBOUNCE
    debounce = function(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }; 

   //SCROLL ANIMATED
   $(function(){
    var $target = $('[data-anime="scroll"],[data-anime-top="scroll-top"],[data-anime-left="scroll-left"],[data-anime-right="scroll-right"],[data-anime-bottom="scroll-bottom"]'),
    animationClass = 'animate',
    offset = $(window).height() * 5/4;
    
    function animeScroll(){
      var documentTop = $(document).scrollTop();
      $target.each(function(){
        var itemTop = $(this).offset().top;
        
        if (documentTop > itemTop - offset){
          $(this).addClass(animationClass);
        } else {
          $(this).removeClass(animationClass);
        }
      });
    }

      animeScroll();
      $(document).scroll(debounce(function(){
        animeScroll();
      }, 200));
      
    });

    // var s = skrollr.init();
    //new WOW().init();
    $(document).ready(function() {
      $('body').ready(function(){
        $('#loading').show();
        setTimeout(function(){
          $('#loading').animate({  opacity: 100 }, {
            step: function(now,fx) {
              $(this).css('transform','translateX('+now+'%)');
              $('#loading').addClass('fadeOut');
            },
            complete: function() {
              $('#loading').hide();
            },
            duration:'slow'
          },600);
        }, 600);
      });
    });  


    $('.slider_home').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1000,
        fade: true,
        arrows: true,
        dots: false,
        autoplaySpeed: 9534,
        autoplay: true,
        cssEase: 'linear',
        nextArrow: '<div class="nextarrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;" xml:space="preserve"><g><g><path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>',
        prevArrow: '<div class="prevarrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 492 492" style="enable-background:new 0 0 492 492;" xml:space="preserve"><g><g><path d="M198.608,246.104L382.664,62.04c5.068-5.056,7.856-11.816,7.856-19.024c0-7.212-2.788-13.968-7.856-19.032l-16.128-16.12C361.476,2.792,354.712,0,347.504,0s-13.964,2.792-19.028,7.864L109.328,227.008c-5.084,5.08-7.868,11.868-7.848,19.084c-0.02,7.248,2.76,14.028,7.848,19.112l218.944,218.932c5.064,5.072,11.82,7.864,19.032,7.864c7.208,0,13.964-2.792,19.032-7.864l16.124-16.12c10.492-10.492,10.492-27.572,0-38.06L198.608,246.104z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>'
    });


})(jQuery);  
