<!DOCTYPE html>
<html lang="pt_BR">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    <?php
    if(is_home()):

      bloginfo('name');

    elseif(is_category()):

      single_cat_title(); echo ' - ' ; bloginfo('name');

    elseif(is_single()):

      single_post_title(); echo ' - ' ; bloginfo('name');

    elseif(is_page()):

      single_post_title(); echo ' - ' ; bloginfo('name');

    else:

      wp_title('',true);

    endif;

    ?>
  </title>
  <meta name="robots" content="index, follow"/>

  <!-- Favicon -->
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-16.png" sizes="16x16" type="image/png">
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-32.png" sizes="32x32" type="image/png">
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-48.png" sizes="48x48" type="image/png">
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-62.png" sizes="62x62" type="image/png">
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon-192.png" sizes="192x192" type="image/png">

  <!-- Depêndencias -->
  <style type="text/css">

   <?php 

   $url = get_bloginfo('template_directory') . '/dist/css/style.css';

   $ch = curl_init();

   curl_setopt ($ch, CURLOPT_URL, $url);

   curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5); 

   curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

   $contents = curl_exec($ch);

   if(curl_errno($ch)):

     echo curl_error($ch);

     $contents = '';

   else:

     curl_close($ch);

   endif;

   if(!is_string($contents) || !strlen($contents)):

     $contents = '';

 endif;

 $contents = str_replace('../img/', get_bloginfo('template_directory') . '/dist/img/', $contents);

 $contents = str_replace('../fonts/', get_bloginfo('template_directory') . '/dist/fonts/', $contents);

 echo $contents;
 ?>
</style>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />


</head>
<body>
  <!-- <div id="loading">
    <div class="animacao">
      <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">

      <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
      s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
      c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>

      <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
      C22.32,8.481,24.301,9.057,26.013,10.047z"><animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 20 20" to="360 20 20"          dur="0.5s" repeatCount="indefinite"/> </path>
    </svg>
  </div>
</div> -->

<header id="linkagem">
  <div class="row no-gutters">
    <div class="container  position-relative displayflex">
      <div class="menu_mobile">
        <div class="menu-toggle">
          <div class="one"></div>
          <div class="two"></div>
          <div class="three"></div>
          <div class="titulo_menu">Menu</div>
        </div><!-- /menu-toggle -->
        <nav class="hidden">
          <div class="logo">
            <a href="<?php echo home_url(); ?>">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo-branco.png">    
            </a>
          </div><!-- /logo -->
          <ul role="navigation">
            <li><a href="#sobre">Sobre nós</a></li>
            <li><a href="#servicos">Serviços e Soluções</a></li>
            <li><a href="#contato">Contato</a></li>
          </ul>
        </nav>
      </div><!-- /menu_mobile -->


      <div class="logo">
        <a href="<?php echo home_url(); ?>">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo-branco.png">  
        </a>
      </div>
      <div class="menu-desktop">
        <ul>
          <li><a href="#sobre">Sobre nós</a></li>
          <li><a href="#servicos">Serviços e Soluções</a></li>
          <li><a href="#contato">Contato</a></li>
        </ul>
      </div><!-- /menu-desktop -->
    </div><!-- /col -->
  </div>  <!-- row -->
</div>

</header> <!-- header-home -->


